// Power and Light controller for aeroponics system.
// by Michael J. Hammel <http://www.graphics-muse.org>
// --------------------------------------------------------------------
// Designed for use with Arduino Pro Mini 328 3.3V/8MHz
// https://www.sparkfun.com/products/11114
// and the following add on boards:
// Sainsmart 4 channel relay
// JBtek TinyRTC DS1307
// Addicore ESP8266
// --------------------------------------------------------------------
// Wire Library: http://arduino.cc/en/Reference/Wire
// Arduino Mini Pro: https://www.arduino.cc/en/Main/ArduinoBoardProMini
// The timer library: http://playground.arduino.cc/Code/Timer
//   Schedule events on ticks to check the real time
// DS1307RTC libs:
//   Time: http://www.pjrc.com/teensy/td_libs_Time.html
//   DS1307RTC: http://www.pjrc.com/teensy/td_libs_DS1307RTC.html
// --------------------------------------------------------------------
// Built for the command line using Arduino Makefile:
//   http://ed.am/dev/make/arduino-mk
// --------------------------------------------------------------------
// Created 12 September 2015
// This example code is in the public domain.
// --------------------------------------------------------------------
// Current design:
// 4 channel relay controls 4 power outlets.
// 3 outlets used for lighting.
// 1 outlet used for pumps.
// Hardcoded pump interval.
// Hardcoded (1 on and 1 off) lamp schedule.
// --------------------------------------------------------------------

#include <DS1307RTC.h>
#include <Time.h>
#include <Timer.h>
#include <Wire.h>

#define OFF HIGH
#define ON  LOW

/* Arduino's LED pin */
int led = 13;

// Static schedules
time_t lampOn;          // On time
time_t lampOff;         // Off time
time_t dayStart;        // Start of the day
time_t dayEnd;          // End of the day
int    lampState;       // Desired state of lamps
int    outletState[4];  // Current state of outlets
long   pumpSched;       // How long to run, in seconds
int    pumpState;       // Current state of pumps
time_t pump;            // When the pump was started
int    pumpInterval;    // How often to toggle the pumps, in minutes

// Instrumentation, for debugging
int pumpChanges = 0;
int lampChanges = 0;
int lastPump = 0;
int lastLamp = 0;

/* Outlets are controlled by these pins on the Arduino. */
int outlet1 = 3;
int outlet2 = 4;
int outlet3 = 5;
int outlet4 = 6;

// Buffers
String currentTime = String("");

// Static strings
char   TOGGLE_PUMPS[] = "Toggling pumps";
String RTC_NOSYNC     = "Unable to sync with the RTC";
String RTC_SYNC       = "RTC has set the system time";

const char *monthName[12] = {
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
};

// Timer object
Timer t;

// Print a string to the serial port.
// Used primarily for debug.
void display(String msg) 
{
    String buf = "[" + currentTime;
    buf += "] ";
    buf += msg;
    Serial.println( buf );
}

// Initialize the RTC to the time the code was compiled.
bool initRTC()
{
    int Hour, Min, Sec;
    char Month[12];
    int Day, Year;
    uint8_t monthIndex;
    tmElements_t tm;

    // Convert time value to tm structure.
    if (sscanf(__TIME__, "%d:%d:%d", &Hour, &Min, &Sec) != 3) 
        return false;
    tm.Hour = Hour;
    tm.Minute = Min;
    tm.Second = Sec;

    // Convert date value to tm structure.
    if (sscanf(__DATE__, "%s %d %d", Month, &Day, &Year) != 3) 
        return false;
    for (monthIndex = 0; monthIndex < 12; monthIndex++) 
    {
        if (strcmp(Month, monthName[monthIndex]) == 0) 
            break;
    }
    if (monthIndex >= 12) 
        return false;
    tm.Day = Day;
    tm.Month = monthIndex + 1;
    tm.Year = CalendarYrToTm(Year);

    // Initialize the RTC.
    RTC.write(tm);
    return true;
}

// Make any single digit into zero padded two digits.
String make2Digits(int number) 
{
    String digits = "";
    if (number >= 0 && number < 10) 
        digits = digits + 0;
    digits = digits + number;
    return digits;
}

/*
 * Enable pins we use
 */
void setPins()
{
    // Pins used for power outlets
    pinMode(outlet1, OUTPUT);     // Enable outlet1 pin.
    pinMode(outlet2, OUTPUT);     // Enable outlet2 pin.
    pinMode(outlet3, OUTPUT);     // Enable outlet3 pin.
    pinMode(outlet4, OUTPUT);     // Enable outlet4 pin.

    // Arduino LED pin used to show operation.
    pinMode(led, OUTPUT);
}

/*
 * Set today's on/off times for the lamps.
 * The results is a time_t value for lampOn and lampOff
 * for today.
 */
void setLampTimes()
{
    tmElements_t tmptm;
    time_t today = now();

    // Set the on/off time for lamps
    // The hour/minute for start/stop should be user selectable!
    breakTime(today, tmptm);
    tmptm.Hour    = 7;
    tmptm.Minute  = 0;
    lampOn        = makeTime(tmptm);
    tmptm.Hour    = 21;
    tmptm.Minute  = 0;
    lampOff        = makeTime(tmptm);

    // Set start and end of current day
    if ( lampOff > lampOn )
    {
        tmptm.Hour    = 0;
        tmptm.Minute  = 0;
        dayStart      = makeTime(tmptm);
        tmptm.Hour    = 23;
        tmptm.Minute  = 59;
        dayEnd        = makeTime(tmptm);
    }
    else
    {
        tmptm.Hour    = 23;
        tmptm.Minute  = 59;
        dayEnd        = makeTime(tmptm);

        // Bump up a day to get tomorrows midnight.
        today += (time_t)((time_t)24*(time_t)60*(time_t)60);
        breakTime(today, tmptm);
        tmptm.Hour    = 0;
        tmptm.Minute  = 0;
        dayStart      = makeTime(tmptm);
    }
}

/*
 * Turn the lights on or off
 * Note: outlet[0] is for the pumps.
 */
void lights()
{
    int i, pin;

    // Map the desired lamp state to the equivalent pin state.
    for(i=0; i<3; i++)
    {
        switch(i)
        {
            case 0: pin = outlet2; break;
            case 1: pin = outlet3; break;
            case 2: pin = outlet4; break;
        }

        if ( outletState[i+1] != lampState )
        {
            digitalWrite(pin, lampState);
            outletState[i+1] = lampState;
        }
    }
}

/*
 * Turn the pumps on or off
 * Outlet[0] is for the pumps.
 */
void pumps()
{
    int pinState;

    // Toggle the pump state.
    if ( pumpState == ON )
        pinState = OFF;
    else
        pinState = ON;

    pump = now();
    digitalWrite(outlet1, pinState);
    outletState[0] = pinState;
    pumpState = pinState;
}

/*
 * Check the time and update scheduled state changes.
 * We check this so often (and fast) that we can afford
 * to assume we will check exactly on an hour/minute boundary.
 */
void checkTime()
{
    int newState;
    String buf;

    // Start in our current state.  We'll change if needed.
    newState = lampState;

    // Read current time and test if event states need to change.
    time_t n = now();
    setLampTimes();

    // If we're on 24/7....
    if ( lampOn == lampOff )
    {
        lampState = ON;
        lights();
        return;
    }

    // If we start early and end late...
    if ( lampOff > lampOn )
    {
        if ( (n>=lampOn) && (n<lampOff) )
        {
            newState = ON;
        }
        else
        {
            newState = OFF;
        }
    }
    else
    {
        // If we start late and end early (passing midnight in the process)...
        if ( ((n>=lampOn) && (n<=dayEnd)) || ((n>=dayStart) && (n<lampOff)) )
        {
            newState = ON;
        }
        else
        {
            newState = OFF;
        }
    }

    if ( newState != lampState )
    {
        lampChanges++;
        buf = "lampState changed to ";
        if ( lampState == ON ) buf += "On";
        else                   buf += "Off";
        buf += ":";
        buf += lampChanges;
        buf += " changes";
        display(buf);
        lampState = newState;
        lights();
    }

    // Test if pump needs to be toggled
    int elapsed = (n - pump)/60;
    if ( elapsed >= pumpInterval )
    {
        char str[32];
        pumpChanges++;
        sprintf(str, "%s: %4d changes", TOGGLE_PUMPS, pumpChanges);
        display( str );
        pumps();
    }
}

/*
 * Read the current temperate value.
 */
void updateTemp()
{
    // TBD
}

/*
 * Retrieve new configuration settings via WiFi.
 */
void getSettings()
{
    // TBD
}

/*
 * Update the attached display.
 * Eventually this will be an LCD display.
 * For now, it's just the serial console.
 */
void updateDisplay()
{
    int i;
    String buf;

    // Show the time
    time_t t = now();
    currentTime = make2Digits(hour(t));
    currentTime = currentTime + ":";
    currentTime = currentTime + make2Digits(minute(t));
    currentTime = currentTime + ":";
    currentTime = currentTime + make2Digits(second(t));

    if ( (lastLamp != lampChanges) || (lastPump != pumpChanges) )
    {
        buf = "Lamp state: ";
        if ( lampState == ON ) buf += "On";
        else                   buf += "Off";
        display( buf );

        for(i=0; i<4; i++)
        {
            int j = i + 1;
            buf = "Outlet ";
            buf += j;
            buf += " state: ";
            if ( outletState[i] == ON ) buf += "On";
            else                        buf += "Off";
            display( buf );
        }
        lastLamp = lampChanges;
        lastPump = pumpChanges;
    }
}

void setup()
{
    /*
     * ---------------------------------------
     * Initialization of variables and state
     * ---------------------------------------
     */
    if ( initRTC() != true )
        Serial.println("Unable to initialize RTC - using existing RTC settings.");

    // start serial output to pass via USB
    Serial.begin(115200);    
    while (!Serial) ; // wait for serial
    delay(200);

    // Sync Time.h library with RTC
    // This allows use of the simpler Time.h API for time comparisons.
    setSyncProvider(RTC.get);
    if(timeStatus()!= timeSet)
    {
       Serial.println( RTC_NOSYNC );
       return;
    }
    else
       Serial.println( RTC_SYNC );

    // Enable pins we use.
    setPins();

    // Initialize lamp states
    lampState      = OFF;
    outletState[1] = OFF;
    outletState[2] = OFF;
    outletState[3] = OFF;
    digitalWrite(outlet2, OFF);
    digitalWrite(outlet3, OFF);
    digitalWrite(outlet4, OFF);

    // Turn the pumps on.
    pumpInterval = 30;
    pump = now();
    pumpState = ON;
    outletState[0] = pumpState;
    digitalWrite(outlet1, ON);

    /*
     * ---------------------------------------
     * Schedule event handling
     * ---------------------------------------
     */

    // Flash the Adruino LED once a second to show operation
    t.oscillate(led, 1000, OFF);

    // Set schedule for checking timed events.
    t.every(2000, checkTime);

    // Set schedule for updating temperature settings.
    // t.every(150, updateTemp);

    // Set schedule for retrieving new settings via WiFi
    // t.every(5000, getSettings);

    // Set schedule for updating the display
    updateDisplay();
    String buf = "Starting time";
    display( buf );
    t.every(1000, updateDisplay);
}

/*
 * The main loop uses the Timer library to run scheduling updates.
 */
void loop()
{
    t.update();
}
